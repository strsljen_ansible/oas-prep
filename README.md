# OpenStack-Ansible preparation automation

In order to sucessfully use OAS, hosts need to be prepared.
This simple automation will help you prepare hosts.

This automation is to be run from ansible control host which is not part of OAS

Linux distribution: Ubuntu

Minimal Ansible version required: 2.3

Prerequieite packages to be installed on hosts before automation can be run:
- python
- python-apt

It follows documentation at:
https://docs.openstack.org/project-deploy-guide/openstack-ansible/ocata/app-config-test.html

## Prerequisites

* OS: Ubuntu 16.04 LTS
* minimal Ansible version: 2.3
* packages on hosts:
  - python
  - python-apt

inventory/hosts file have to be adjusted to your needs. Please see the example provided.

## Usage

### Configuration

#### Ansible inventory
First of all, configure inventory/hosts file to match your environment.
Ansible groups follow:
- oas-controller: controller/infra node(s)
- oas-compute: compute node(s)
- oas-storage: storage node(s)

In the example given we have:
- 1 controller node (infra1)
- 1 compute node (compute1)
- 1 storage node (storage1)

#### Group vars

##### group_vars/oas

Common vars for all OAS nodes.

Configure here physical interface to be used for OAS.
In this example we are using eth1.

    oas_interface

OAS branch depends on OpenStack version you will use. In this example we are using
stable/ocata

    oas_git_branch

The options are:
- stable/mitaka
- stable/newton
- stable/ocata

Note: if this variable is not set, Ansible will default it to "master" branch, which
is probably not what you want.

Network setup
Here we define some IP addresses used later in hosts_vars and also in template when
generating openstack_user_config.yml for OAS deployment on infra1 node

    oas_infra1_mgmt_ip
    oas_compute1_mgmt_ip
    oas_storage1_mgmt_ip
    oas_storage1_iscsi_ip

##### group_vars/oas-controller

Group vars for controller group.
Contains flags which network bridges to set IPs on

##### group_vars/oas-compute

Group vars for controller group.
Contains flags which network bridges to set IPs on

##### group_vars/oas-storage

Group vars for controller group.
Contains flags which network bridges to set IPs on

#### Host vars
For each host we define IP addresses to be set (where apropriate)
Not all hosts need IP addresses on all 3 bridges.
Example for compute1 node:

```
oas_mgmt_ip: "172.29.236.12"
oas_mgmt_mask: "255.255.252.0"
oas_mgmt_gw: "172.29.236.1"
oas_mgmt_dns: "8.8.8.8 8.8.4.4"

oas_vxlan_ip: "172.29.240.12"
oas_vxlan_mask: "255.255.252.0"

oas_storage_ip: "172.29.244.12"
oas_storage_mask: "255.255.252.0"
```

### Running the automation

```
ansible-playbook prepare_hosts.yml -D
```
We need to reboot hosts after new network configuration is added
```
ansible -a "reboot" oas
```
